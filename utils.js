async function sleep(timeout) {
  return new Promise(resolve => setTimeout(resolve, timeout));
}

async function retry(asyncFunction, timeout = 1000, log = true) {
  try {
    return await asyncFunction();
  } catch (error) {
    if (log) {
      console.log("Error:", error, "Retrying in:", timeout);
    }
    await sleep(timeout);
    return await retry(asyncFunction, timeout * 2, log);
  }
}

module.exports = {
  retry,
};