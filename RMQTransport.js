const Transport = require("winston-transport");
const amqp = require("amqplib");
const { retry } = require("./utils");

class RMQTransport extends Transport {
  /**
   * @param {string} applicationName
   * @param {object} amqpConfiguration
   */
  constructor(applicationName, amqpConfiguration) {
    super();

    this.name = "RMQTransport";
    this._applicationName = applicationName;
    this._channel = null;
    this._exchangeName = amqpConfiguration.exchange;
    /*
     * We are using a queue because on application start we cannot be sure that
     * the rabbitmq connection is established. So we are putting the message into
     * the queue and publish them as soon as the connection is ready.
     */
    this._queue = [];
    this._initializeRabbit(amqpConfiguration).catch(error =>
      console.error("Error establishing the connection:", error)
    );

    this.setMaxListeners(30);
  }

  /**
   * Adds a log to the internal queue and tries to push it to RabbitMq.
   *
   * @param {string} level the log level of the message
   * @param {string} msg the actual message
   * @param {object} meta some additional data which we want to append to the log
   * @param {function} callback a callback function which we have to call after the messages have been published
   * @returns {Promise<void>}
   */
  async log(info, callback) {
    setImmediate(() => this.emit("logged", info));
    const { level, message, ...meta } = info;
    const data = {
      logTimestamp: new Date().toISOString(),
      level,
      message,
      "@customFields": meta
    };

    this._queue.push(data);
    await this._publishMessages();
    if (callback && typeof callback === "function") {
      callback();
    }
  }

  /**
   * Tries to publish all messages.
   * If there is no amqp connection yet it will just return.
   *
   * @returns {Promise<void>}
   * @private
   */
  async _publishMessages() {
    if (this._channel === null) {
      //if connection was not created try creating connection again
      return;
    }
    console.log(" QUESUES",this._queue);
    const queueWorker = this._queue.map(message => {
      this._publishMessage(message);
    });
    this._queue = [];
    const responses = await Promise.all(queueWorker);
    const unsuccessfulMessages = responses.filter(
      response => response !== null
    );
    this._queue = [...this._queue, ...unsuccessfulMessages];
    console.log("UNPLISHED QUESUES",this._queue);
  }


  async _publishMessage(message) {
    if (!this._channel === null) {
      return;
    }

    const data = this._populateServiceInformation(message);

    try {
      await this._channel.publish(
        this._exchangeName,
        "*",
        Buffer.from(JSON.stringify(data))
      );
      console.log(" messages published==> ", data);

      return null;
    } catch (error) {
      return message;
    }
  }


  _populateServiceInformation(message) {
    const data = Object.assign({}, message);
    data.application = this._applicationName;
    if (data.channel === undefined) {
      data.channel = RMQTransport.DEFAULT_CHANNEL;
    }
    return data;
  }

  /**
   * Initializes the amqp connection and opens the channel.
   *
   * @param {object} amqpConfiguration
   * @private
   */
  async _initializeRabbit(amqpConfiguration) {
    const connection = await retry(() => amqp.connect(amqpConfiguration));

    const channel = await retry(() => connection.createChannel());

    channel.assertExchange(amqpConfiguration.exchange, "topic", {
      durable: true,
      autoDelete: false
    });

    this._channel = channel;

    if (
      amqpConfiguration.onConnect &&
      typeof amqpConfiguration.onConnect === "function"
    ) {
      amqpConfiguration.onConnect(this);
    }

    if (this._queue.length > 0) {
      this._publishMessages();
    }
  }
}

RMQTransport.DEFAULT_CHANNEL = "other";

module.exports = RMQTransport;
