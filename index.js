const config = require("config");
const Logger = require("./logger");
const RMQTransport = require("./RMQTransport");
const express = require("express");

const connectionOptions = config.get("rmq");
const amqpTransport = new RMQTransport("Logging Service", connectionOptions);
const logger = new Logger({
  environment: "development",
  transports: [amqpTransport]
});

const port = 3333;

const app = express();

const router = express.Router();


const handleLog = async (req, res) => {
  const { logType, message, customData } = req.body;

  const { headers } = req;

  const data = {
    customData: { ...(customData || {}), headers }
  };

  req.logger.log(logType, message, data);

  res.send();
};

const asyncAdapter=(
  handler
) =>{
  return async (req, res, next) => {
      try {
          await handler(req, res, next);
      } catch (error) {
          next(error);
      }
  }
};

router.post("/logs", asyncAdapter(handleLog));

app.use((req, res, next)=>{
  req.logger = logger;
  next();
})

app.use(router);

app.listen(port, () => {
  console.log(`Logging Service started on PORT:: ${port}`);
});


