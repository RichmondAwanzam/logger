const winston = require("winston");

class Logger {

  constructor(config = {}) {
    this._environment = config.environment || process.env.NODE_ENV;
    this._transports = config.transports || [];
    this._callbacks = config.callbacks || {};
    this._level = config.level || Logger.LOG_TYPE_DEBUG;

    this._logger = this._createWinstonLogger();
  }

  info(message, customData = {}) {
    this.log(Logger.LOG_TYPE_INFO, message, customData);
  }

  debug(message, customData = {}) {
    this.log(Logger.LOG_TYPE_DEBUG, message, customData);
  }

  error(message, customData = {}) {
    this.log(Logger.LOG_TYPE_ERROR, message, customData);
  }


  warn(message, customData = {}) {
    this.log(Logger.LOG_TYPE_WARNING, message, customData);
  }

  log(level, message, customData = {}) {
    if (!Logger.LogTypes.includes(level)) {
      console.error("Logger cannot log level:", level);
      return;
    }

    let customDataForLog;

    if (customData instanceof Error) {
      customDataForLog = {
        error_message: customData.message,
        error_stack: customData.stack,
      };
    } else if (typeof customData !== "object") {
      customDataForLog = { custom_data: customData };
    } else {
      customDataForLog = Object.assign({}, customData);
    }

    customDataForLog.environment = this._environment;

    this._logger.log({ ...customDataForLog, level, message });

    const callback = this._callbacks[level];
    if (typeof callback === "function") {
      callback(message, customData);
    }
  }


  _createWinstonLogger() {
    return winston.createLogger({
      level: this._level,
      transports: this._transports,
    });
  }
}
Logger.LOG_TYPE_INFO = "info";
Logger.LOG_TYPE_ERROR = "error";
Logger.LOG_TYPE_WARNING = "warn";
Logger.LOG_TYPE_DEBUG = "debug";
Logger.LogTypes = [
  Logger.LOG_TYPE_INFO,
  Logger.LOG_TYPE_ERROR,
  Logger.LOG_TYPE_WARNING,
  Logger.LOG_TYPE_DEBUG,
];

module.exports = Logger;
