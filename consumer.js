const amqp = require("amqplib");
const connection = await retry(() => amqp.connect(amqpConfiguration));

const channel = await retry(() => connection.createChannel());

channel.assertExchange(amqpConfiguration.exchange, "topic", {
  durable: true,
  autoDelete: false
});